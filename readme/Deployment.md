## Common
1. Create the war file `./gradlew clean build`
2. The war 'web-market-app.war' will be situated in the build/libs folder
3. Create the JETTY_BASE folder
   1. `mkdir JETTY_BASE_FOLDER`
   2. `cd JETTY_BASE_FOLDER`
   3. `java -jar $JETTY_HOME/start.jar --add-module=http,deploy`
   4. Copy the application war in the JETTY_BASE_FOLDER/webapps/
   5. Start Jetty: `java -jar $JETTY_HOME/start.jar`
4. Check the URL in the logs

## Embedded Jetty
1. Create the jar file: `./gradlew clean build`
2. Run it: `java -jar <jarFile>`
3. Check the URL in the logs

## Docker
1. Run: `docker build -t web-market-app .`
2. Create and start the container: `docker run -d web-market-app`
3. Use the app
