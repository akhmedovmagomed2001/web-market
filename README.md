# WebMarket

Web store based on the Java Servlets.

## Technologies

- Java 17
- Gradle
- Servlet API
- Jetty
- PostgreSQL
- Docker

## Guides

You can find many guides on deployment, dockerization and other things in the readme folder
