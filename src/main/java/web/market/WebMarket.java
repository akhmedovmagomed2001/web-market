package web.market;

import com.google.inject.Guice;
import com.google.inject.Injector;
import web.market.module.JettyServerModule;
import web.market.network.JettyServer;
import web.market.service.ConfigService;

public final class WebMarket {

    private WebMarket() {
    }

    public static void main(String[] args) {
        ConfigService.init();
        getJettyServer().start();
    }

    private static JettyServer getJettyServer() {
        Injector jettyInjector = Guice.createInjector(new JettyServerModule());
        return jettyInjector.getInstance(JettyServer.class);
    }
}
