package web.market.network;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import web.market.module.JettyServerModule;

public class JettyServer {

    private static final Logger LOG = LoggerFactory.getLogger(JettyServer.class);

    @Inject
    private Server server;
    @Inject
    @Named(JettyServerModule.PORT_FIELD_NAME)
    private Integer port;
    @Inject
    @Named(JettyServerModule.MAIN_SERVLET_PATH_FIELD_NAME)
    private String mainServletPath;

    public void start() {
        configConnectors();
        configHandlers();

        try {
            server.start();
        } catch (Exception ex) {
            LOG.error("Exception during server startup...", ex);
            throw new RuntimeException(ex);
        }
    }

    private void configConnectors() {
        ServerConnector serverConnector = new ServerConnector(server);
        serverConnector.setPort(port);

        server.addConnector(serverConnector);
    }

    private void configHandlers() {
        ServletContextHandler servletContextHandler = new ServletContextHandler();
        servletContextHandler.addServlet(MainServlet.class, mainServletPath);

        server.setHandler(servletContextHandler);
    }
}
