package web.market.module;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import org.eclipse.jetty.server.Server;
import web.market.service.ConfigService;

import static web.market.constant.ConfigNames.MAIN_SERVLET_PATH_CONFIG;
import static web.market.constant.ConfigNames.PORT_CONFIG;

public class JettyServerModule extends AbstractModule {

    public static final String PORT_FIELD_NAME = "port";
    public static final String MAIN_SERVLET_PATH_FIELD_NAME = "mainServletPath";

    @Override
    protected void configure() {
        Server serverInstance = new Server();
        bind(Server.class).toInstance(serverInstance);

        int port = Integer.parseInt(ConfigService.getConfig(PORT_CONFIG));
        bind(Integer.class).annotatedWith(Names.named(PORT_FIELD_NAME)).toInstance(port);

        String mainServletPath = ConfigService.getConfig(MAIN_SERVLET_PATH_CONFIG);
        bind(String.class).annotatedWith(Names.named(MAIN_SERVLET_PATH_FIELD_NAME)).toInstance(mainServletPath);
    }
}
