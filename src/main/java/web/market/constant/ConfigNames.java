package web.market.constant;

public final class ConfigNames {

    private ConfigNames() {
    }

    public static final String PORT_CONFIG = "PORT";
    public static final String MAIN_SERVLET_PATH_CONFIG = "MAIN_SERVLET_PATH";

}
