package web.market.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ConfigServiceTest {

    private static final String CONFIG_NAME1 = "config1";
    private static final String CONFIG_VALUE1 = "value1";
    private static final String CONFIG_NAME2 = "config2";
    private static final String CONFIG_VALUE2 = "value2";

    @BeforeEach
    void setUp() {
        ConfigService.clean();
    }

    @Test
    void shouldReturnNullConfigValueWithoutInit() {
        String actual = ConfigService.getConfig(CONFIG_NAME1);

        assertThat(actual).isNull();
    }

    @Test
    void shouldGetConfigValue() {
        ConfigService.init();

        String actual1 = ConfigService.getConfig(CONFIG_NAME1);
        assertThat(actual1).isEqualTo(CONFIG_VALUE1);

        String actual2 = ConfigService.getConfig(CONFIG_NAME2);
        assertThat(actual2).isEqualTo(CONFIG_VALUE2);
    }
}
