package web.market.network;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletMapping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import web.market.module.JettyServerModule;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

@ExtendWith(MockitoExtension.class)
class JettyServerTest {

    private static final int PORT = 1234;
    private static final String MAIN_SERVLET_PATH = "mainServletPath";

    private final Server server = spy(new Server());

    private JettyServer testInstance;

    @BeforeEach
    void setUp() throws Exception {
        Injector jettyInjector = Guice.createInjector(new JettyServerTestModule());
        testInstance = jettyInjector.getInstance(JettyServer.class);

        doNothing().when(server).start();
        doReturn(Boolean.FALSE).when(server).isStarted();
    }

    @Test
    void shouldStart() {
        testInstance.start();

        // Assert connectors
        Connector[] actualConnectors = server.getConnectors();
        assertThat(actualConnectors).hasSize(1);

        Connector actualConnector = actualConnectors[0];
        assertThat(actualConnector).isInstanceOf(ServerConnector.class);

        ServerConnector actualServerConnector = (ServerConnector) actualConnector;
        int actualPort = actualServerConnector.getPort();
        assertThat(actualPort).isEqualTo(PORT);

        // Assert handlers
        Handler[] actualHandlers = server.getHandlers();
        assertThat(actualHandlers).hasSize(1);

        Handler actualHandler = actualHandlers[0];
        assertThat(actualHandler).isInstanceOf(ServletContextHandler.class);

        ServletContextHandler actualServletContextHandler = (ServletContextHandler) actualHandler;
        ServletHandler actualServletHandler = actualServletContextHandler.getServletHandler();

        ServletMapping[] actualServletMappings = actualServletHandler.getServletMappings();
        assertThat(actualServletMappings).hasSize(1);

        ServletMapping actualServletMapping = actualServletMappings[0];
        String[] actualPathSpecs = actualServletMapping.getPathSpecs();
        assertThat(actualPathSpecs).hasSize(1);

        String actualPathSpec = actualPathSpecs[0];
        assertThat(actualPathSpec).isEqualTo(MAIN_SERVLET_PATH);
    }

    private class JettyServerTestModule extends AbstractModule {
        @Override
        protected void configure() {
            bind(Server.class).toInstance(server);

            bind(Integer.class)
                    .annotatedWith(Names.named(JettyServerModule.PORT_FIELD_NAME))
                    .toInstance(PORT);

            bind(String.class)
                    .annotatedWith(Names.named(JettyServerModule.MAIN_SERVLET_PATH_FIELD_NAME))
                    .toInstance(MAIN_SERVLET_PATH);
        }
    }
}
