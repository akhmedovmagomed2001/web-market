package web.market.network;

import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MainServletTest {

    private final MainServlet testInstance = new MainServlet();

    @Mock
    private HttpServletResponse httpServletResponse;
    @Mock
    private PrintWriter printWriter;

    @Test
    void shouldDoGet() throws IOException {
        when(httpServletResponse.getWriter()).thenReturn(printWriter);

        testInstance.doGet(null, httpServletResponse);

        String expectedContextType = "text/html";
        verify(httpServletResponse).setContentType(expectedContextType);
        String expectedPrint = "<h2>Hello World!</h2>";
        verify(printWriter).println(expectedPrint);
    }
}
