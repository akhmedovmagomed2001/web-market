FROM gradle:7.5.1-jdk17-alpine AS build
COPY --chown=gradle:gradle . /home/gradle/project
WORKDIR /home/gradle/project
RUN gradle clean build

FROM amazoncorretto:17-alpine3.16
EXPOSE 8080
RUN mkdir /app
COPY --from=build /home/gradle/project/build/libs/*.jar /app/web-market-app.jar
ENTRYPOINT ["java", "-jar", "/app/web-market-app.jar"]
